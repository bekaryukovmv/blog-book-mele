from django.contrib import admin
from .models import Post, Comment
# Register your models here.

# Делаем отображение комментариев под постом в админке
class PostInstanceInline(admin.StackedInline):
    model = Comment
    # количество отображаемых пустых комментариев
    extra = 1


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    # отображаемые поля
    list_display = ('title', 'slug', 'author', 'publish', 'status')
    # Поля фильтра
    list_filter = ('author', 'publish', 'status', 'created')
    # Поля поиска
    search_fields = ('title', 'body')
    # Добавляет автозаполнение slug
    prepopulated_fields = {'slug': ('title',)}
    # Меняет способ указания автора (теперь через поиск)
    raw_id_fields = ('author',)
    # Добавляет ссылки навигации по датам
    date_hierarchy = 'publish'
    # Сортировка
    ordering = ('status', 'publish')
    # Что будет ссылкой?
    list_display_links = ('slug',)
    # Возможность редактировать название не открывая объект
    list_editable = ('title', 'status')
    inlines = (PostInstanceInline, )


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'post', 'created', 'active')
    list_filter = ('active', 'created', 'updated')
    # list_editable = ('active',)
    search_fields = ('name', 'email', 'body')
