# Generated by Django 2.2.6 on 2019-11-01 20:07

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_auto_20191101_2245'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='publish',
            field=models.DateTimeField(default=datetime.datetime(2019, 11, 1, 20, 7, 3, 184028, tzinfo=utc), verbose_name='Дата публикации'),
        ),
    ]
