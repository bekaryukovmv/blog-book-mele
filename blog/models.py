from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse
from taggit.managers import TaggableManager

# Create your models here.

# Создаём свой менеджер моделей. Указывается до самих моделей, его использующих


class PublishedManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(status='published')


class Post(models.Model):
    STATUS_CHOICES = (
        ('draft', 'Draft'),
        ('published', 'Published'),
    )
    title = models.CharField(max_length=250, verbose_name='Заголовок')
    slug = models.SlugField(max_length=250, unique_for_date='publish')
    author = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='blog_posts', verbose_name='Автор')
    body = models.TextField()
    publish = models.DateTimeField(default=timezone.now, verbose_name='Дата публикации')
    print(publish)
    created = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    updated = models.DateTimeField(auto_now=True, verbose_name='Отредактировано')
    status = models.CharField(
        max_length=10, choices=STATUS_CHOICES, default='draft', verbose_name='Статус')
    # указываем менеджеры объектов, если добавили свой
    objects = models.Manager()
    published = PublishedManager()
    tags = TaggableManager()

    class Meta:
        ordering = ['-publish']
        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("blog:post_detail",
                       args=[self.publish.year,
                             self.publish.month,
                             self.publish.day,
                             self.slug]
                       )
    # Возвращает правильную форму для множественного числа.
    def get_plural(self):
        comments = len(self.comments.filter(active=True))
        if 0 <= comments <= 1000:
            x = comments % 10
            y = comments % 100
            if y == 11:
                return 'комментариев'
            elif x == 1 and y != 11:
                return 'комментарий'
            if  y in [12, 13, 14]:
                return 'комментариев'
            elif x in [2, 3, 4] and y not in [12, 13, 14]:
                return 'комментария'
            if x in [0, 5, 6, 7, 8, 9]:
                return 'комментариев'

class Comment(models.Model):
    # related_name позволяет обращаться к комментариям из связанной модели вот так: Post.comments.all()
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comments')
    name = models.CharField(max_length=80, verbose_name='Имя')
    email = models.EmailField()
    body = models.TextField(verbose_name='Комментарий')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    updated = models.DateTimeField(auto_now=True, verbose_name='Отредактировано')
    active = models.BooleanField(default=True)

    class Meta:
        ordering = ['created']
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'

    def __str__(self):
        return f'Комментарий от {self.name} к записи {self.post}'


